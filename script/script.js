"use strict"


const linkCss = document.getElementById('dark-theme');
const themeToggle = document.getElementById('ios-toggle');
const DARK_CSS = './css/dark-theme.css';

themeToggle.addEventListener('click', event => {

    if (event.target.checked) {
        linkCss.setAttribute('href', DARK_CSS)
        localStorage.setItem('theme', 'dark')
    }
    else {
        linkCss.setAttribute('href', '')
        localStorage.setItem('theme', 'light')
    }
});

const saveTheme = localStorage.getItem('theme') ?? 'light';

if (saveTheme === 'light') {
    linkCss.setAttribute('href', '')
}
else {
    linkCss.setAttribute('href', DARK_CSS)
    themeToggle.checked = true;
}
